﻿using System;
namespace StudentService.Models
{
    public class User
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string currentCourse { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}
